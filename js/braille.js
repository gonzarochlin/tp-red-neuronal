var BRAILLE = {
	' ': '⠀',   // space bar to dot-0
	'_': '⠸',
	'-': '⠤',
	',': '⠠',
	';': '⠰',
	':': '⠱',
	'!': '⠮',
	'?': '⠹',
	'.': '⠨',
	'(': '⠷',
	'[': '⠪',
	'@': '⠈',
	'*': '⠡',
	'/': '⠌',
	'\'': '⠄',
	'\"': '⠐',
	'\\': '⠳',
	'&': '⠯',
	'%': '⠩',
	'^': '⠘',
	'+': '⠬',
	'<': '⠣',
	'>': '⠜',
	'$': '⠫',
	'0': '⠴',
	'1': '⠂',
	'2': '⠆',
	'3': '⠒',
	'4': '⠲',
	'5': '⠢',
	'6': '⠖',
	'7': '⠶',
	'8': '⠦',
	'9': '⠔',
	'A': '⠁',
	'B': '⠃',
	'C': '⠉',
	'D': '⠙',
	'E': '⠑',
	'F': '⠋',
	'G': '⠛',
	'H': '⠓',
	'I': '⠊',
	'J': '⠚',
	'K': '⠅',
	'L': '⠇',
	'M': '⠍',
	'N': '⠝',
	'O': '⠕',
	'P': '⠏',
	'Q': '⠟',
	'R': '⠗',
	'S': '⠎',
	'T': '⠞',
	'U': '⠥',
	'V': '⠧',
	'W': '⠺',
	'X': '⠭',
	'Z': '⠵',
	']': '⠻',
	'#': '⠼',
	'Y': '⠽',
	')': '⠾',
	'=': '⠿'
};

// Global variables

var Braille_Binary = {
	'A': [1,0,0,0,0,0],
	'B': [1,0,1,0,0,0],
	'C': [1,1,0,0,0,0],
	'D': [1,1,0,1,0,0],
	'E': [1,0,0,1,0,0],
	'F': [1,1,1,0,0,0],
	'G': [1,1,1,1,0,0],
	'H': [1,0,1,1,0,0],
	'I': [0,1,1,0,0,0],
	'J': [0,1,1,1,0,0],
	'K': [1,0,0,0,1,0],
	'L': [1,0,1,0,1,0],
	'M': [1,1,0,0,1,0],
	'N': [1,1,0,1,1,0],
	'O': [1,0,0,1,1,0],
	'P': [1,1,1,0,1,0],
	'Q': [1,1,1,1,1,0],
	'R': [1,0,1,1,1,0],
	'S': [0,1,1,0,1,0],
	'T': [0,1,1,1,1,0],
	'U': [1,0,0,0,1,1],
	'V': [1,0,1,0,1,1],
	'W': [0,1,1,1,0,1],
	'X': [1,1,0,0,1,1],
	'Y': [1,1,0,1,1,1],
	'Z': [1,0,0,1,1,1],
	' ': [0,0,0,0,0,0],
	'1': [1,0,0,0,0,0],
	'2': [1,0,1,0,0,0],
	'3': [1,1,0,0,0,0],
	'4': [1,1,0,1,0,0],
	'5': [1,0,0,1,0,0],
	'6': [1,1,1,0,0,0],
	'7': [1,1,1,1,0,0],
	'8': [1,0,1,1,0,0],
	'9': [0,1,1,0,0,0],
	'0': [0,1,1,1,0,0],
};

var alphabetBinaryJson = {};

// Braille functions

function initialize() {
	alphabetBinaryJson = getBinaryJson();

	var inputLayer = new Layer(7);
	var hiddenLayer = new Layer(7);
	var outputLayer = new Layer(6);

	inputLayer.project(hiddenLayer);
	hiddenLayer.project(outputLayer);

	return new Network({
		input: inputLayer,
		hidden: [hiddenLayer],
		output: outputLayer
	});
}

function getBinaryJson() {
	let array = [];
	let binaryJson = {};
	for (var key in Braille_Binary) {
		const ascii = key.charCodeAt();
		let binary = ascii.toString(2);
		if(binary.length === 6){
			binary = '0' + binary;
		}
		array = binary.split('');
		binaryJson[key]= array.map(bit => parseInt(bit));
	}
	return binaryJson;
}

function train(network) {
	// network.clear();
	const trainer = new Trainer(network);
	const trainingSet = [];
	for (var key in Braille_Binary) {
		trainingSet.push(
			{
				input: alphabetBinaryJson[key],
				output: Braille_Binary[key]
			}
		)
	}
	
	trainer.train(trainingSet,{
		rate: .1,
		iterations: 70000,
		error: .005,
		shuffle: false,
		log: 1000,
		cost: Trainer.cost.CROSS_ENTROPY
	});
}

function getBrailleArray(network, string) {
	const array = [];
	let braille, char;
	for(var i = 0; i < string.length; i++) {
		char = string[i];
		if(!Braille_Binary.hasOwnProperty(char)){
			return null;
		}
		braille = network.activate(alphabetBinaryJson[char]);
		array.push(braille.map(val => Math.round(val)));
	}
	return array;
}

// HTML functions

function getBrailleHtml(array) {
	let char;
	let html = '<ul id="braille">';
	for(var i = 0; i < array.length; i++) {
		html += '<li class="char">';
		char = array[i];
		for(var j = 0; j < char.length; j++) {
			if(char[j]) {
				html += '<div class="circle one"></div>';
			} else { 
				html += '<div class="circle zero"></div>';
			}
			if(j % 2) {
				html += '</br>';
			}
		}
		html += '</li>';
	}
	html += '</ul>';
	return html;
}